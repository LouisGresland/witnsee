<header>
    <nav>
        <ul>
            <li><a href="{{ route('main') }}">Accueil</a></li>
            <span id="separator"></span>
            @if(!Auth::check())
                <li><a href="{{ route('signup') }}">S'inscrire</a></li>
                <li><a href="{{ route('signin') }}">Se connecter</a></li>
            @else
                <li><a href="{{ url('/logout') }}">Se déconnecter</a></li>
            @endif
        </ul>
    </nav>
</header>
