<a href="{{ $url }}" class="{{ $class or 'btn btn-info btn-xs' }}">
    <span aria-hidden="true"><i class="fa fa-edit"></i></span>
    Modifier
</a>
