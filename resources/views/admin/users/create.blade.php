@extends('layouts.admin')

@section('content')
<div class="row">

	@if(Session::has('error')) 
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
@endif
		<div class="col-md-8 col-md-offset-2">
			<h1>Créer un nouvel utilisateur</h1>
			<hr>

			{!! Form::open(array('route' => 'admin.users.store', 'data-parsley-validate' => '', 'onsubmit' => 'return validateForm()')) !!}
				{{ Form::label('last_name', 'Nom:') }}
				{{ Form::text('last_name', null, array('class' => 'form-control', 'maxlength' => '255')) }}
				<p id="error_last_name" class="error"></p>

				{{ Form::label('first_name', 'Prénom:') }}
				{{ Form::text('first_name', null, array('class' => 'form-control', 'maxlength' => '255') ) }}
				<p id="error_first_name" class="error"></p>

				{{ Form::label('email', 'E-mail:') }}
				{{ Form::email('email', null, array('class' => 'form-control', 'maxlength' => '255') ) }}
				<p id="error_mail" class="error"></p>

				{{ Form::label('password', 'Mot de passe:') }}
				{{ Form::text('password', null, array('class' => 'form-control', 'maxlength' => '255') ) }}
				<p id="error_password" class="error"></p>

				{{ csrf_field() }}
				<br/>
				{{ Form::submit('Créer', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
</div>
<script type="text/javascript">
function validateForm() {
	var last_name = document.getElementById("last_name").value;
    
    if(last_name == '') {
    	
        
        document.getElementById("last_name").style.borderColor = "red";
        document.getElementById("error_last_name").innerHTML = "Veuillez renseigner correctement le nom de l'utilisateur";

    } else {
    	document.getElementById("last_name").style.borderColor = "#eee";
        document.getElementById("error_last_name").innerHTML = "";
        
    }

    var first_name = document.getElementById("first_name").value;

    if(first_name == '') {
    	
        
        document.getElementById("first_name").style.borderColor = "red";
        document.getElementById("error_first_name").innerHTML = "Veuillez renseigner correctement le prénom de l'utilisateur";

    } else {
    	document.getElementById("first_name").style.borderColor = "#eee";
        document.getElementById("error_first_name").innerHTML = "";
        
    }

    var mail = document.getElementById("email").value;

    var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
     
    if(reg.test(mail)) {
    	document.getElementById("email").style.borderColor = "#eee";
        document.getElementById("error_mail").innerHTML = "";

    } else {
    	
        document.getElementById("email").style.borderColor = "red";
        document.getElementById("error_mail").innerHTML = "Veuillez renseigner correctement votre adresse mail";
        email = 'faux';
    }

    

	var password = document.getElementById("password").value;
	var length = password.length;

    if(password == '' || length < 6 ) {
    	
        
        document.getElementById("password").style.borderColor = "red";
        document.getElementById("error_password").innerHTML = "Veuillez renseigner correctement le mot de passe, il doit contenir au moins 6 caractères";
        password = 'faux';

    } else {
    	document.getElementById("password").style.borderColor = "#eee";
        document.getElementById("error_password").innerHTML = "";
        
    }

    if(last_name == '' || first_name == '' || email == 'faux' || password == 'faux'){
    	return false;
	}
}
</script>
@endsection