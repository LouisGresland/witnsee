@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif
<div class="row">
		<div class="col-md-8">


			<div id="backend-comments" style="margin-top: 50px;">

				<table class="table">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Prénom</th>
							<th>Email</th>
							<th width="70px"></th>
						</tr>
					</thead>
					<tr>
							<td>{{ $user->last_name }}</td>
							<td>{{ $user->first_name }}</td>
							<td>{{ $user->email }}</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-4">
			<div class="well">
				
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('admin.users.edit', 'Modifier', array($user->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::open(['route' => ['admin.users.destroy', $user->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-block', 'onclick' => 'return ConfirmDelete()']) !!}

						{!! Form::close() !!}
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						{{ Html::linkRoute('admin.users.index', '<< Voir tous les utilisateurs', array(), ['class' => 'btn btn-default btn-block btn-h1-spacing']) }}
					</div>
				</div>

			</div>
		</div>
	</div>
<script>

  function ConfirmDelete()
  {
  var x = confirm("Etes vous sûr de vouvloir supprimmer cet utilisateur?");
  if (x)
    return true;
  else
    return false;
  }

</script>

@endsection