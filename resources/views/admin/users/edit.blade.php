@extends('layouts.admin')

@section('content')
<div class="row">
		{!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT', 'onsubmit' => 'return validateForm()']) !!}
		<div class="col-md-8">
			{{ Form::label('last_name', 'Nom:') }}
			{{ Form::text('last_name', null, ["class" => 'form-control input-lg']) }}
			<p id="error_last_name" class="error"></p>

			{{ Form::label('first_name', 'Prénom:') }}
			{{ Form::text('first_name', null, ["class" => 'form-control input-lg']) }}
			<p id="error_first_name" class="error"></p>

			{{ Form::label('email', 'E-mail:') }}
			{{ Form::text('email', null, ["class" => 'form-control input-lg']) }}
			<p id="error_mail" class="error"></p>  
               
			<td><input type="hidden" name="email" value="{{ $user->email }}"></td>
			{{ Form::label('user', 'Utilisateur') }}
			<td><input type="checkbox" {{ $user->hasRole('User') ? 'checked' : '' }} name="role_user"></td><br/>
			{{ Form::label('gerant', 'Gérant de lieu') }}
			<td><input type="checkbox" {{ $user->hasRole('Gerant') ? 'checked' : '' }} name="role_gerant"></td><br/>
			{{ Form::label('prestataire', 'Prestataire') }}
			<td><input type="checkbox" {{ $user->hasRole('Prestataire') ? 'checked' : '' }} name="role_prestataire"></td><br/>
			{{ Form::label('admin', 'Administrateur') }}
			 <td><input type="checkbox" {{ $user->hasRole('Admin') ? 'checked' : '' }} name="role_admin"></td><br/>
			 {{ csrf_field() }}
			
		</div>

		<div class="col-md-4">

			<div class="well">

				<div class="row">

					<div class="col-sm-6">
						{!! Html::linkRoute('admin.users.show', 'Annuler', array($user->id), array('class' => 'btn btn-danger btn-block')) !!}
					</div>
					<div class="col-sm-6">
						{{ Form::submit('Enregistrer', ['class' => 'btn btn-success btn-block']) }}
					</div>
				</div>

			</div>
				
		</div>
{!! Form::close() !!}
<br/>
	
<script type="text/javascript">
function validateForm() {
	var last_name = document.getElementById("last_name").value;
    
    if(last_name == '') {
    	
        
        document.getElementById("last_name").style.borderColor = "red";
        document.getElementById("error_last_name").innerHTML = "Veuillez renseigner correctement le nom de l'utilisateur";

    } else {
    	document.getElementById("last_name").style.borderColor = "#eee";
        document.getElementById("error_last_name").innerHTML = "";
        
    }

    var first_name = document.getElementById("first_name").value;

    if(first_name == '') {
    	
        
        document.getElementById("first_name").style.borderColor = "red";
        document.getElementById("error_first_name").innerHTML = "Veuillez renseigner correctement le prénom de l'utilisateur";

    } else {
    	document.getElementById("first_name").style.borderColor = "#eee";
        document.getElementById("error_first_name").innerHTML = "";
        
    }

    var mail = document.getElementById("email").value;

    var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
     
    if(reg.test(mail)) {
    	document.getElementById("email").style.borderColor = "#eee";
        document.getElementById("error_mail").innerHTML = "";

    } else {
    	
        document.getElementById("email").style.borderColor = "red";
        document.getElementById("error_mail").innerHTML = "Veuillez renseigner correctement votre adresse mail";
        email = 'faux';
    }


    if(last_name == '' || first_name == '' || email == 'faux'){
    	return false;
	}
}
</script>

@stop
