@extends('layouts.admin')

@section('content')
<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Créer un nouvel utilisateur</h1>
			<hr>

			{!! Form::open(array(['route' => 'admin.users.store', 'data-parsley-validate' => '', 'onsubmit' => 'return confirm("Are you sure?")'])) !!}
				{{ Form::label('last_name', 'Nom:') }}
				{{ Form::text('last_name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
				

				{{ Form::label('first_name', 'Prénom:') }}
				{{ Form::text('first_name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255') ) }}
				

				{{ Form::label('email', 'E-mail:') }}
				{{ Form::email('email', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255') ) }}
				

				{{ Form::label('password', 'Mot de passe:') }}
				{{ Form::text('password', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255') ) }}
				
				{{ csrf_field() }}
				<br/>
				{{ Form::submit('Créer', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
</div>

@endsection