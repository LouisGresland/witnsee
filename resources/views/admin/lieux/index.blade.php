@extends('layouts.admin')

@section('content')
<div class="row">
		<div class="col-md-10">
			<h1>Tous les utilisateurs</h1>
		</div>

		<div class="col-md-2">
			<a href="{{ route('admin.users.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Ajouter un utilisateur</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Email</th>
					
					<th></th>
				</thead>

				<tbody>
					
					@foreach ($users as $user)
						
						<tr>
							<th>{{ $user->last_name }}</th>
							<td>{{ $user->first_name }}</td>
							<td>{{ $user->email }}</td>
							
							<td><a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-default btn-sm">Voir</a> <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-default btn-sm">Modifier</a></td>
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{!! $users->links(); !!}
			</div>
		</div>
</div>

@endsection