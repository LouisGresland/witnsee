@extends('layouts.admin')

@section('content')
<div class="row">
		{!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT']) !!}
		<div class="col-md-8">
			{{ Form::label('last_name', 'Nom:') }}
			{{ Form::text('last_name', null, ["class" => 'form-control input-lg']) }}
			<p id="error_last_name" class="error"></p>

			{{ Form::label('first_name', 'Prénom:') }}
			{{ Form::text('first_name', null, ["class" => 'form-control input-lg']) }}
			<p id="error_first_name" class="error"></p>

			{{ Form::label('email', 'E-mail:') }}
			{{ Form::text('email', null, ["class" => 'form-control input-lg']) }}
			<p id="error_mail" class="error"></p>  
               

                   
			
		</div>

		<div class="col-md-4">

			<div class="well">

				<div class="row">

					<div class="col-sm-6">
						{!! Html::linkRoute('admin.users.show', 'Annuler', array($user->id), array('class' => 'btn btn-danger btn-block')) !!}
					</div>
					<div class="col-sm-6">
						{{ Form::submit('Enregistrer', ['class' => 'btn btn-success btn-block']) }}
					</div>
				</div>

			</div>
				
		</div>
{!! Form::close() !!}
<div id="Form_role">
	<form action="{{ route('admin.assign') }}" method="post">
			<td><input type="hidden" name="email" value="{{ $user->email }}"></td>
			{{ Form::label('user', 'Utilisateur') }}
			<td><input type="checkbox" {{ $user->hasRole('User') ? 'checked' : '' }} name="role_user"></td><br/>
			{{ Form::label('gerant', 'Gérant de lieu') }}
			<td><input type="checkbox" {{ $user->hasRole('Gerant') ? 'checked' : '' }} name="role_gerant"></td><br/>
			{{ Form::label('prestataire', 'Prestataire') }}
			<td><input type="checkbox" {{ $user->hasRole('Prestataire') ? 'checked' : '' }} name="role_prestataire"></td><br/>
			{{ Form::label('admin', 'Administrateur') }}
			 <td><input type="checkbox" {{ $user->hasRole('Admin') ? 'checked' : '' }} name="role_admin"></td><br/>
			 {{ csrf_field() }}
			 <td><button type="submit">Rôle assigné</button></td>
         </form>
</div>

@stop
