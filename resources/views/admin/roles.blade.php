<form action="{{ route('admin.assign') }}" method="post">
			<td><input type="hidden" name="email" value="{{ $user->email }}"></td>
			{{ Form::label('user', 'Utilisateur') }}
			<td><input type="checkbox" {{ $user->hasRole('User') ? 'checked' : '' }} name="role_user"></td><br/>
			{{ Form::label('gerant', 'Gérant de lieu') }}
			<td><input type="checkbox" {{ $user->hasRole('Gerant') ? 'checked' : '' }} name="role_gerant"></td><br/>
			{{ Form::label('prestataire', 'Prestataire') }}
			<td><input type="checkbox" {{ $user->hasRole('Prestataire') ? 'checked' : '' }} name="role_prestataire"></td><br/>
			{{ Form::label('admin', 'Administrateur') }}
			 <td><input type="checkbox" {{ $user->hasRole('Admin') ? 'checked' : '' }} name="role_admin"></td><br/>
			 {{ csrf_field() }}
			 <td><button type="submit">Rôle assigné</button></td>
         </form>
