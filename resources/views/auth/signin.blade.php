@extends('layouts.master')

@section('content')
    <form action="{{ route('signin') }}" method="post" onsubmit="return validateConnection()">
        <div class="input-group">
            <label for="email">E-mail</label>
            <input type="email" id="email" name="email">
            <p id="error_mail" class="error"></p>
        </div>
        <div class="input-group">
            <label for="password">Mot de passe</label>
            <input type="password" id="password" name="password">
        </div>
        <label>
        <input type="checkbox" name="remember"> Se rappeler
        </label>

        
        {{ csrf_field() }}
        <button type="submit">Se connecter</button>
    </form>
    <a class="btn btn-link" href="{{ url('/password/reset') }}">Mot de passe oublié?</a>

    <script type="text/javascript">
function validateConnection() {
    
    var mail = document.getElementById("email").value;
     
    if(mail == '')  {

        document.getElementById("email").style.borderColor = "red";
        document.getElementById("error_mail").innerHTML = "Veuillez renseigner correctement votre adresse mail";
        return false;

    } else {


        $('#email').css("border-color","#eee");        
        document.getElementById("error_mail").innerHTML = "";
    }
}
    </script>
@endsection