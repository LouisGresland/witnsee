@extends('layouts.master')

@section('content')
    <form action="{{ route('signup') }}" method="post" onsubmit="return validateForm()">
        
        <div class="input-group">
            <label for="last_name">Nom</label>
            <input type="text" id="last_name" name="last_name">
            <p id="error_last_name" class="error"></p>
        </div>
        <div class="input-group">
            <label for="first_name">Prénom</label>
            <input type="text" id="first_name" name="first_name">
            <p id="error_first_name" class="error"></p>
        </div>
        <div class="input-group">
            <label for="email">E-mail</label>
            <input type="email" id="email" name="email">
            <p id="error_mail" class="error"></p>
        </div>
        <div class="input-group">
            <label for="password">Mot de passe</label>
            <input type="password" id="password" name="password">
            <p id="error_password" class="error"></p>
        </div>
       {{ csrf_field() }} 
        <button type="submit">S'inscrire</button>
    </form>
<script type="text/javascript">
function validateForm() {
    var last_name = document.getElementById("last_name").value;
    
    if(last_name == '') {
        
        
        document.getElementById("last_name").style.borderColor = "red";
        document.getElementById("error_last_name").innerHTML = "Veuillez renseigner correctement le nom de l'utilisateur";

    } else {
        document.getElementById("last_name").style.borderColor = "#eee";
        document.getElementById("error_last_name").innerHTML = "";
        
    }

    var first_name = document.getElementById("first_name").value;

    if(first_name == '') {
        
        
        document.getElementById("first_name").style.borderColor = "red";
        document.getElementById("error_first_name").innerHTML = "Veuillez renseigner correctement le prénom de l'utilisateur";

    } else {
        document.getElementById("first_name").style.borderColor = "#eee";
        document.getElementById("error_first_name").innerHTML = "";
        
    }

    var mail = document.getElementById("email").value;

    var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
     
    if(reg.test(mail)) {
        document.getElementById("email").style.borderColor = "#eee";
        document.getElementById("error_mail").innerHTML = "";

    } else {
        
        document.getElementById("email").style.borderColor = "red";
        document.getElementById("error_mail").innerHTML = "Veuillez renseigner correctement votre adresse mail";
        email = 'faux';
    }

    

    var password = document.getElementById("password").value;
    var length = password.length;

    if(password == '' || length < 6 ) {
        
        
        document.getElementById("password").style.borderColor = "red";
        document.getElementById("error_password").innerHTML = "Veuillez renseigner correctement le mot de passe, il doit contenir au moins 6 caractères";
        password = 'faux';

    } else {
        document.getElementById("password").style.borderColor = "#eee";
        document.getElementById("error_password").innerHTML = "";
        
    }

    if(last_name == '' || first_name == '' || email == 'faux' || password == 'faux'){
        return false;
    }
}
    </script>
@endsection