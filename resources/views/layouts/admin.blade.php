<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Portail d'administration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
{{ Html::style('css/styles.css') }}
</head>
<body>
<a href="{{ route('admin.users.index') }}"> Utilisateurs </a><br/>
<a href=""> Lieux </a>
<div class="main">


    @yield('content')

</div>
</body>
</html>