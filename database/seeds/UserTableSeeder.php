<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'User')->first();
        $role_prestataire = Role::where('name', 'Prestataire')->first();
        $role_admin = Role::where('name', 'Admin')->first();
        $role_gerant = Role::where('name', 'Gérant')->first();


        $user = new User();
        $user->first_name = 'Victor';
        $user->last_name = 'Visitor';
        $user->email = 'visitor@example.com';
        $user->password = bcrypt('visitor');
        $user->save();
        $user->roles()->attach($role_user);

        $admin = new User();
        $admin->first_name = 'Alex';
        $admin->last_name = 'Admin';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $prestataire = new User();
        $prestataire->first_name = 'Andy';
        $prestataire->last_name = 'Prestataire';
        $prestataire->email = 'prestataire@example.com';
        $prestataire->password = bcrypt('prestataire');
        $prestataire->save();
        $prestataire->roles()->attach($role_prestataire);

        $gerant = new User();
        $gerant->first_name = 'Andy';
        $gerant->last_name = 'Gérant';
        $gerant->email = 'gerant@example.com';
        $gerant->password = bcrypt('gerant');
        $gerant->save();
        $gerant->roles()->attach($role_gerant);
    }
}
