<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'User';
        $role_user->description = 'Un simple utilisateur';
        $role_user->save();

        $role_prestataire = new Role();
        $role_prestataire->name = 'Prestataire';
        $role_prestataire->description = 'Un prestataire';
        $role_prestataire->save();

        $role_admin = new Role();
        $role_admin->name = 'Admin';
        $role_admin->description = 'Un admin';
        $role_admin->save();

        $role_gerant = new Role();
        $role_gerant->name = 'Gérant';
        $role_gerant->description = 'Un gérant de lieu';
        $role_gerant->save();
    }
}
