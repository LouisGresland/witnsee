<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Role;

use Session;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(15);
        return view('admin/users/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
        'last_name'         => 'required|max:255',
        'first_name'        => 'required|max:255',
        'email'             => 'required|email|unique:users',
        'password'          => 'required',
            ));
        // store in the database
        $user = new User();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->save();
        $user->roles()->attach(Role::where('name', 'User')->first());

        
        
        Session::flash('success', 'L\'utilisateur a bien été ajouté!');
        
        return view('admin.users.show')->withUser($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        
        return view('admin.users.show')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        
        return view('admin.users.edit')->withUser($user);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = user::find($id);
       
        $this->validate($request, array(
        'last_name'         => 'required|max:255',
        'first_name'        => 'required|max:255',
        'email'             => 'required|email|unique:users,email,'.$user->id,
            ));
        
        // Save the data to the database
        $user->last_name = $request->last_name;
        $user->first_name = $request->first_name;
        $user->email = $request->email;
        
        $user->save(); 

        $user->roles()->detach();
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'User')->first());
        }
        if ($request['role_gerant']) {
            $user->roles()->attach(Role::where('name', 'Gerant')->first());
        }
        if ($request['role_prestataire']) {
            $user->roles()->attach(Role::where('name', 'Prestataire')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'Admin')->first());
        } 

        $message = 'Les modifications ont bien été enregistré.';
             
        // set flash data with success message
        Session::flash('success', 'Les modifications ont bien été enregistré.');
        // redirect with flash data to users.show
        $user = User::find($id);
        
        return view('admin.users.show')->withUser($user);
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        Session::flash('success', 'L\'utilisateur a bien été supprimé.');
        $users = User::paginate(15);
        return view('admin/users/index', compact('users'));
    }
}
