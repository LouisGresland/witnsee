<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function getIndex()
    {
        return view('index');
    }

    public function getAdminPage()
    {
        $users = User::all();
        return view('admin.index', ['users' => $users]);
    }
    public function getAdminPageRole($id)
    {
        $users = User::find($id);
        return view('admin.role', ['users' => $users]);
    }
    public function getPrestatairePage()
    {
        return view('prestataire');
    }
    public function getGerantPage()
    {
        return view('gerant');
    }

    public function getGenerateArticle()
    {
        return response('Article generated!', 200);
    }
    
    public function postAdminAssignRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        $user->roles()->detach();
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'User')->first());
        }
        if ($request['role_gerant']) {
            $user->roles()->attach(Role::where('name', 'Gerant')->first());
        }
        if ($request['role_prestataire']) {
            $user->roles()->attach(Role::where('name', 'Prestataire')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'Admin')->first());
        }
        return redirect()->back();
    }
}