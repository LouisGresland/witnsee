<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('index');
    })->name('main');

    Route::get('/gérantdelieu', [
        'uses' => 'AppController@getGerantPage',
        'as' => 'gerant',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Gerant']
    ]);

    Route::get('/prestataire', [
        'uses' => 'AppController@getPrestatairePage',
        'as' => 'prestataire',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Prestataire']
    ]);

    Route::get('/admin', [
        'uses' => 'AppController@getAdminPage',
        'as' => 'admin',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('/admin/roles', [
        'uses' => 'RoleController@editrole',
        'as' => 'admin.roles'
        
    ]);

    Route::post('/admin/assign-roles', [
        'uses' => 'AppController@postAdminAssignRoles',
        'as' => 'admin.assign'
    ]);


    Route::get('/signup', [
        'uses' => 'AuthController@getSignUpPage',
        'as' => 'signup'
    ]);

    Route::post('/signup', [
        'uses' => 'AuthController@postSignUp',
        'as' => 'signup'
    ]);

    Route::get('/signin', [
        'uses' => 'AuthController@getSignInPage',
        'as' => 'signin'
    ]);

    Route::post('/signin', [
        'uses' => 'AuthController@postSignIn',
        'as' => 'signin'
    ]);

    Route::get('/logout', [
        'uses' => 'AuthController@getLogout',
        'as' => 'logout'
    ]);

        Route::resource('admin/users', 'UserController');

        Route::auth();

});



        